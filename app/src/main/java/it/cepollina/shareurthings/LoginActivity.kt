package it.cepollina.shareurthings

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.email_field
import kotlinx.android.synthetic.main.activity_login.password_field
import kotlinx.android.synthetic.main.activity_sign_up.*

class LoginActivity: AppCompatActivity() {


    val TAG = "LOGIN ACTIVITY"
    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()


        link_signup.setOnClickListener{
            startActivity(Intent(applicationContext, SignUpActivity::class.java))
        }

        button_login.setOnClickListener{
            if (email_field.text.contains(Regex("[^@]+@[^.]+\\..+")))
                login()
            else
                Toast.makeText(this, "Inserire una mail nel formato corretto", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Permette di eseguire il login a un utente utilizzando le credenziali utilizzate per registrarsi
     */
    fun login(){
        auth.signInWithEmailAndPassword(email_field.text.toString(), password_field.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithEmail:success")
                    Toast.makeText(this, "Login effettuato", Toast.LENGTH_LONG).show()
                    startActivity(Intent(applicationContext, HomeActivity::class.java))
                } else {
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

}