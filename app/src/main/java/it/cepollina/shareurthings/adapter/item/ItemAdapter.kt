package it.cepollina.shareurthings.adapter.item

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import it.cepollina.shareurthings.R

class ItemAdapter(private val context: Context, private var imageList: ArrayList<ImageCustom>) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val returnedView: View
        val holder : ViewHolder

        if (convertView == null){
            holder = ViewHolder()
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            returnedView = inflater.inflate(R.layout.item_base, null, true)
            holder.nameItem = returnedView.findViewById(R.id.name_item_value)
            holder.availabilityItem = returnedView.findViewById(R.id.availability_item_value)
            holder.imageView = returnedView.findViewById(R.id.imageView)
            holder.dateExpire = returnedView.findViewById(R.id.date_expire_item_base)
            holder.keys = returnedView.findViewById(R.id.keys_item_base)

            returnedView.tag = holder

        }else{
            holder = convertView.tag as ViewHolder
            returnedView = convertView
        }


        holder.imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageList[position].image, 0,  imageList[position].image.size))
        holder.nameItem.text = imageList[position].name
        holder.availabilityItem.text = imageList[position].availability
        holder.dateExpire.text = imageList[position].expiredDate
        holder.keys.text = imageList[position].keys

        return returnedView
    }

    override fun getItem(position: Int): Any {
        return imageList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return imageList.size
    }

    private inner class ViewHolder {
        lateinit var nameItem: TextView
        lateinit var availabilityItem: TextView
        lateinit var imageView: ImageView
        lateinit var dateExpire: TextView
        lateinit var keys: TextView
    }
}