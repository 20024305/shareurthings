package it.cepollina.shareurthings.adapter.item

class ImageCustom(
    var image: ByteArray, val name: String,
    val availability: String, val idOwner: String,
    val idItem:String, val latitude: Double,
    val longitude: Double, val expiredDate: String,
    val keys:String)
