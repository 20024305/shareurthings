package it.cepollina.shareurthings.adapter.share

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import it.cepollina.shareurthings.R
import it.cepollina.shareurthings.adapter.item.ItemAdapter

class ShareAdapter(private val context: Context, private var shareList: ArrayList<ShareCustom>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val returnedView: View
        val holder: ViewHolder

        if (convertView == null){
            holder = ViewHolder()
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            returnedView = inflater.inflate(R.layout.share_base, null, true)
            holder.nameItem = returnedView.findViewById(R.id.name_item_share_list)
            holder.startShareAt = returnedView.findViewById(R.id.startAt_share_list)
            holder.endShareAt = returnedView.findViewById(R.id.endAt_share_list)
            holder.state = returnedView.findViewById(R.id.state_share_list)
            holder.role = returnedView.findViewById(R.id.role_share_list)

            returnedView.tag = holder
        }else{
            holder = convertView.tag as ViewHolder
            returnedView = convertView
        }

        holder.nameItem.text = shareList[position].nameItem
        holder.startShareAt.text = shareList[position].startShareAt
        holder.endShareAt.text = shareList[position].endShareAt
        holder.state.text = shareList[position].state
        holder.role.text = shareList[position].role

        return returnedView
    }

    override fun getItem(position: Int): Any {
        return shareList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return shareList.size
    }

    private inner class ViewHolder {
        lateinit var nameItem: TextView
        lateinit var startShareAt: TextView
        lateinit var endShareAt: TextView
        lateinit var state: TextView
        lateinit var role: TextView
    }
}