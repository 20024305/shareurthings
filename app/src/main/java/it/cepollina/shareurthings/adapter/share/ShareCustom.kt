package it.cepollina.shareurthings.adapter.share

class ShareCustom(
    val idItem: String, var nameItem:String,
    val idShare: String, val startShareAt: String,
    val endShareAt: String, val ratingShare: String,
    val state: String, val idOwnerItem: String,
    val role: String)