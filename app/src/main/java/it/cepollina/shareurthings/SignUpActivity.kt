package it.cepollina.shareurthings

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity: AppCompatActivity(){

    val TAG = "SIGN UP ACTIVITY"
    lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        auth = FirebaseAuth.getInstance()
        button_signup.setOnClickListener{
            if (email_field.text.contains(Regex("[^@]+@[^.]+\\..+")) && fieldComplete())
                addUser()
            else
                Toast.makeText(this, "Inserire una mail nel formato corretto o campi non compilati", Toast.LENGTH_LONG).show()
        }
    }



    /**
     * Permette di salvare le informazioni riguardanti l'utente che si registra alla piattaforma, legando l'id
     * dell'account con le informazioni dell'utente
     */
    private fun addUserInfo() {
        val info = hashMapOf<String, Any?>(
                "idUser" to auth.currentUser!!.uid,
                "name" to name_field.text.toString(),
                "surname" to surname_field.text.toString())

        db.collection("users")
                .add(info)
                .addOnSuccessListener { documentReference ->
                    Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error adding document", e)
                }
    }

    /**
     * Permette di creare un nuovo account legando l'email con la password di un determinato utente
     */
    fun addUser(){
        auth.createUserWithEmailAndPassword(email_field.text.toString(), password_field.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "createUserWithEmail:success")
                    /*Toast.makeText(this, "userId: ${auth.currentUser!!.uid}", Toast.LENGTH_LONG).show()*/
                    addUserInfo()
                    finish()
                } else {
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    /**
     * Metodo per il controllo se i campi sono tutti completi
     */
    private fun fieldComplete(): Boolean{
        return !email_field.text.isBlank() &&
                !password_field.text.isBlank() &&
                !name_field.text.isBlank() &&
                !surname_field.text.isBlank()
    }
}
