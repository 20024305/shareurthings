package it.cepollina.shareurthings.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.FirebaseStorage
import it.cepollina.shareurthings.R
import it.cepollina.shareurthings.adapter.item.ImageCustom
import it.cepollina.shareurthings.adapter.item.ItemAdapter
import it.cepollina.shareurthings.adapter.share.ShareAdapter
import it.cepollina.shareurthings.adapter.share.ShareCustom
import it.cepollina.shareurthings.ui.items.ItemInfo
import it.cepollina.shareurthings.ui.share.ShareFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.items_fragment.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    val db = FirebaseFirestore.getInstance()
    private lateinit var listDocument: Task<QuerySnapshot>
    val settings = FirebaseFirestoreSettings.Builder()
        .setTimestampsInSnapshotsEnabled(true)
        .build()
    val listItems = ArrayList<ShareCustom> ()

    val auth = FirebaseAuth.getInstance()


    val TAG = "HOME_F"

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        getShares()
    }

    override fun onStart() {
        super.onStart()
    }


    /**
     * Metodo di recupero degli share appartenenti all'utente che ha effettuato l'accesso dal database. Gli share
     * prelevati sono quelli che sono stati avviati dall'utente: tutti gli share prelevati da questo metodo impostano
     * come ruolo dell'user a "Utilizzatore", nel senso che ha preso in prestito un item.
     * Il campo ruolo è utilizzato solo per dare una informazione in più che può visionare dalla lista degli share,
     * non è utilizzato in nessun altro modo.
     * Viene controllato che non siano presenti lo stesso numero di items nella lista, in modo da non dover ricaricare dati dal
     * database.
     */
    private fun getShares(){
        db.setFirestoreSettings(settings)
        listDocument = db.collection("shares")
            .whereEqualTo("idOwnerShare", auth.currentUser!!.uid)
            .get()
            .addOnSuccessListener {
                val tmp = it.documents
                if (tmp.size == listItems.size){
                    getListShare()
                }else{
                    listItems.clear()
                    for (i in 0..tmp.size - 1) {
                        val obj = tmp.get(i).data
                        listItems.add(
                            ShareCustom(
                                obj!!.get("idItem").toString(),
                                "",
                                obj.get("idShare").toString(),
                                obj.get("startShare").toString(),
                                obj.get("endShare").toString(),
                                obj.get("rating").toString(),
                                obj.get("state").toString(),
                                obj.get("idOwnerItem").toString(),
                                "Utilizzatore dell'item")
                        )
                    }
                }
            }.addOnFailureListener {
                Log.d(TAG, "------------Errore recupero dati")
            }
            .addOnCompleteListener {
                getSharesOwnerRole()
            }
    }

    /**
     * Metodo di recupero degli share appartenenti all'utente che ha effettuato l'accesso dal database. Gli share
     * prelevati sono quelli che coinvolgono un oggetto messo a disposizione dall'user: tutti gli share prelevati
     * impostano come ruolo dell'user "Proprietario", nel senso che questo è proprietario dell'item. Il campo ruolo è utilizzato solo per dare una informazione in più che può visionare dalla lista degli share,
     * non è utilizzato in nessun altro modo.
     */
    private fun getSharesOwnerRole(){
        db.setFirestoreSettings(settings)
        listDocument = db.collection("shares")
            .whereEqualTo("idOwnerItem", auth.currentUser!!.uid)
            .get()
            .addOnSuccessListener {
                val tmp = it.documents
                for (i in 0..tmp.size - 1) {
                    val obj = tmp.get(i).data
                    listItems.add(
                        ShareCustom(
                            obj!!.get("idItem").toString(),
                            "",
                            obj.get("idShare").toString(),
                            obj.get("startShare").toString(),
                            obj.get("endShare").toString(),
                            obj.get("rating").toString(),
                            obj.get("state").toString(),
                            obj.get("idOwnerItem").toString(),
                            "Proprietario dell'item")
                    )
                }

            }.addOnFailureListener {
                Log.d(TAG, "------------Errore recupero dati")
            }
            .addOnCompleteListener {
                getNameItem()
            }
    }

    /**
     * Metodo di recupero delle informazioni riguardanti l'item coinvolto nello share in modo che si possa mostrare
     * il nome di esso.
     */
    private fun getNameItem(){
        for (i in 0 .. listItems.size-1){
            db.collection("items")
                .whereEqualTo("idItems", listItems.get(i).idItem)
                .get()
                .addOnSuccessListener {
                    if(it.documents.size == 1){
                        val item = it.documents.get(0).data
                        listItems.get(i).nameItem = item!!.get("name").toString()
                    }else{
                        listItems.get(i).nameItem = "Oggetto rimosso dal proprietario"
                    }
                }.addOnFailureListener {
                    Log.d(TAG, "-------------------------Errore recupero nome item")
                }.addOnCompleteListener {
                    getListShare()
                }
        }
    }

    /**
     * Metodo che, dato la lista degli share, mostri la lista di essi all'utente. Inoltre è messo su ogni
     * elemento della lista un listener che permette di passare al fragment relativo alle informazioni specifiche
     * dello share.
     */
    private fun getListShare(){
        val shareAdapter = ShareAdapter(
            this.requireContext(),
            listItems
        )
        listView_home.adapter = shareAdapter
        listView_home.setOnItemClickListener { parent, view, position, id ->
            setFragmentResult("idShare",
                bundleOf("idShare" to listItems.get(position).idShare)
            )
            val shareFragment = ShareFragment()
            val fragmentTransaction = this.parentFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.nav_host_fragment, shareFragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
    }
}
