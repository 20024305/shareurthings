package it.cepollina.shareurthings.ui.share

import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.setFragmentResultListener
import com.google.android.gms.maps.model.VisibleRegion
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.FirebaseStorage
import it.cepollina.shareurthings.R
import it.cepollina.shareurthings.adapter.item.ImageCustom
import kotlinx.android.synthetic.main.fragment_share.*
import java.time.LocalDate

class ShareFragment : Fragment() {
    val db = FirebaseFirestore.getInstance()
    val settings = FirebaseFirestoreSettings.Builder()
        .setTimestampsInSnapshotsEnabled(true)
        .build()

    val auth = FirebaseAuth.getInstance()
    val storageRef = FirebaseStorage.getInstance().getReference()

    var idItem: String? = null
    var idOwnerItem: String? = null
    var idShare: String? = null

    val TAG = "ITEM_INFO_F"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_share, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStart() {
        super.onStart()
        setFragmentResultListener("idShare") {key, bundle ->
            idShare = bundle.getString("idShare")
            getShareInfo()
        }

        button_finish_share_info.setOnClickListener {
            if (rating_user_picker_share_info.text.equals("-")){
                endShare(false)
            }else{
                if(rating_user_picker_share_info.text.toString().toInt() < 0 || rating_user_picker_share_info.text.toString().toInt() > 5)
                    Toast.makeText(requireContext(), "Inserire un valore che sia tra 0 e 5", Toast.LENGTH_LONG).show()
                else
                    endShare(true)
            }
        }
        button_valutate_owner_share_info.setOnClickListener {
            if(rating_owner_picker_share_info.text.toString().toInt() < 0 || rating_user_picker_share_info.text.toString().toInt() > 5)
                    Toast.makeText(requireContext(), "Inserire un valore che sia tra 0 e 5", Toast.LENGTH_LONG).show()
                else
                    valutateShare()
        }
    }


    /**
     * Metodo di recupero delle informazioni riguardanti uno share dal database. Lo share prelevato è quello
     * selezionato dall'utente per essere visionato.
     * Dopo aver recuperato le informazioni imposta i vari campi relativi per la visualizzazione delle informazioni.
     * Inoltre vengono fatti i vari controlli per permettere la valutazione da parte dell'utilizzatore dell'item
     * e da parte del proprietario dell'item e per l'impostazione dei vari campi che vengano visualizzati nel modo corretto.
     */
    private fun getShareInfo(){
        db.setFirestoreSettings(settings)
        db.collection("shares")
            .whereEqualTo("idShare", idShare)
            .get()
            .addOnSuccessListener {
                if (it.documents.size == 1){
                    val idDocument = it.documents.get(0).id
                    val share = it.documents.get(0).data
                    state_share_info.text = share!!.get("state").toString()
                    if(state_share_info.text.equals("Terminato")){
                        rating_user_picker_share_info.isEnabled = false
                        button_finish_share_info.visibility = View.INVISIBLE
                        if(auth.currentUser!!.uid == share.get("idOwnerItem")){
                            rating_owner_picker_share_info.isEnabled = true
                            button_valutate_owner_share_info.visibility = View.VISIBLE
                        }
                    }else{
                        rating_owner_picker_share_info.visibility = View.INVISIBLE
                        rating_owner_picker_label_share_info.visibility = View.INVISIBLE
                        if(auth.currentUser!!.uid == share.get("idOwnerItem")){
                            disableAllRatingField()
                        }
                    }
                    date_start_share_share_info.text = share.get("startShare").toString()
                    date_finish_share_share_info.text = share.get("endShare").toString()
                    rating_user_picker_share_info.setText(share.get("rating_user").toString())
                    rating_owner_picker_share_info.setText(share.get("rating_owner").toString())
                    idItem = share.get("idItem").toString()
                    idOwnerItem = share.get("idOwnerItem").toString()
                }
            }
            .addOnFailureListener {
                Log.d(TAG, "-------------------------Errore recupero info base")
            }.addOnCompleteListener {
                getItemInfo(idItem!!, idOwnerItem!!)
            }
    }


    /**
     * Metodo di recupero delle informazioni dell'item legato al prestito dal database. Dopo aver recuperato
     * le informazioni imposta i campi relativi alla visualizzazione delle informazioni.
     */
    private fun getItemInfo(idItem: String, idOwner: String){
        var removed: Boolean = false
        db.collection("items")
            .whereEqualTo("idItems", idItem)
            .get()
            .addOnSuccessListener {
                if (it.documents.size == 1){
                    val item = it.documents.get(0).data
                    name_share_info.text = item!!.get("name").toString()
                    keys_item_share_info.text = item.get("keys").toString()
                    idOwnerItem = item.get("idOwner").toString()
                }else{
                    name_share_info.text = "Oggetto eliminato dal proprietario"
                    keys_item_share_info.text = "Oggetto eliminato dal proprietario"
                    removed = true
                }
            }
            .addOnFailureListener {
                Log.d(TAG, "--------------------------------Errore recupero info proprietario")
            }
            .addOnCompleteListener {
                getUserInfo(idOwner, removed)
            }
    }


    /**
     * Metodo di recupero delle informazioni dal database riguardanti il proprietario dell'item.
     * Dopo aver recuperato le informazioni imposta i vari campi relativi per la visualizzazione
      */
    private fun getUserInfo(idOwnerItem: String, removed: Boolean){
        db.collection("users")
            .whereEqualTo("idUser", idOwnerItem)
            .get()
            .addOnSuccessListener {
                if (it.documents.size == 1){
                    val owner = it.documents.get(0).data
                    val infoOwner = owner!!.get("name").toString() + " " + owner.get("surname").toString()
                    owner_share_info.text = infoOwner
                }
            }
            .addOnFailureListener {
                Log.d(TAG, "--------------------------------Errore recupero info proprietario")
            }
            .addOnCompleteListener {
                if(!removed){
                    getImage(idOwnerItem)
                }
            }
    }


    /**
     * Metodo di recupero dell'immagine dal database di proprietà dell'item che si visualizza.
     * Dopo aver recuperato questa si imposta il campo previsto per la visione di essa.
     */
    private fun getImage(idOwner: String){
        val islandRef =  storageRef.child(idOwner + "/" + idItem)
        val ONE_MEGABYTE: Long = 1024 * 1024 * 5
        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeByteArray(it, 0,  it.size)
            image_share_info.setImageBitmap(bitmap)
        }.addOnFailureListener {
            // Handle any errors
            Log.d(TAG, "------------Errore recupero immagine")
        }
    }


    /**
     * Metodo di aggiornamento delle informazioni necessarie al confermare la fine di uno share.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun endShare(ratingValue: Boolean) {
        db.collection("shares")
            .whereEqualTo("idShare", idShare)
            .get()
            .addOnSuccessListener { documents ->
                if(documents.size() == 1){
                    documents.documents.get(0)
                        .reference
                        .update("endShare", LocalDate.now().toString())
                        .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!") }
                        .addOnFailureListener { e -> Log.w(TAG, "---------------Errore caricamento modifica fine share", e) }
                    documents.documents.get(0)
                        .reference
                        .update("state", "Terminato")
                        .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!") }
                        .addOnFailureListener { e -> Log.w(TAG, "---------------Errore caricamento modifica fine share", e) }
                    if (ratingValue){
                        documents.documents.get(0)
                            .reference
                            .update("rating_user", rating_user_picker_share_info.text.toString())
                            .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!") }
                            .addOnFailureListener { e -> Log.w(TAG, "---------------Errore caricamento modifica fine share", e) }
                    }
                }
            }
            .addOnCompleteListener {
                db.collection("items")
                    .whereEqualTo("idItems", idItem)
                    .get()
                    .addOnSuccessListener { documents ->
                        if(documents.size() == 1) {
                            documents.documents.get(0)
                                .reference
                                .update("availability", true)
                                .addOnCompleteListener {
                                    Log.d(
                                        TAG,
                                        "DocumentSnapshot successfully updated!"
                                    )
                                }
                                .addOnFailureListener { e ->
                                    Log.w(
                                        TAG,
                                        "---------------Errore caricamento modifica fine share",
                                        e
                                    )
                                }
                        }
                    }.addOnCompleteListener {
                        button_finish_share_info.visibility = View.INVISIBLE
                    }
            }
    }


    /**
     * Metodo di aggiornamento della valutazione dello share da parte del proprietario dell'oggetto
     */
    private fun valutateShare() {
        db.collection("shares")
            .whereEqualTo("idShare", idShare)
            .get()
            .addOnSuccessListener { documents ->
                if (documents.size() == 1) {
                    documents.documents.get(0)
                        .reference
                        .update("rating_owner", rating_owner_picker_share_info.text.toString())
                        .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!") }
                        .addOnFailureListener { e -> Log.w(TAG, "---------------Errore caricamento modifica fine share", e) }
                }
            }.addOnCompleteListener {
                rating_owner_picker_share_info.isEnabled = false
                button_valutate_owner_share_info.visibility = View.INVISIBLE
            }

    }

    /**
     * Metodo per la disabilitazione alla visualizzazione di alcuni campi dell'interfaccia
     */
    private fun disableAllRatingField(){
        rating_user_picker_share_info.visibility = View.INVISIBLE
        rating_user_picker_label_share_info.visibility = View.INVISIBLE
        rating_owner_picker_share_info.visibility = View.INVISIBLE
        rating_owner_picker_label_share_info.visibility = View.INVISIBLE
        button_finish_share_info.visibility = View.INVISIBLE
    }
}