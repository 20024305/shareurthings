package it.cepollina.shareurthings.ui.dashboard

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.FirebaseStorage


import it.cepollina.shareurthings.R
import it.cepollina.shareurthings.adapter.item.ImageCustom
import it.cepollina.shareurthings.adapter.item.ItemAdapter
import it.cepollina.shareurthings.ui.items.ItemInfo
import kotlinx.android.synthetic.main.fragment_dashboard.*
import java.io.IOException
import java.time.LocalDate

class DashboardFragment : Fragment(), OnMapReadyCallback {

    private lateinit var dashboardViewModel: DashboardViewModel
    private lateinit var mMap: GoogleMap

    var latitude: Double = 0.0
    var longitude: Double = 0.0

    private val LOCATION_REQUEST_CODE = 101
    var currentLocation : LatLng? = null



    val db = FirebaseFirestore.getInstance()
    val settings = FirebaseFirestoreSettings.Builder()
        .setTimestampsInSnapshotsEnabled(true)
        .build()
    val listItems = ArrayList<ImageCustom> ()

    val auth = FirebaseAuth.getInstance()
    val storageRef = FirebaseStorage.getInstance().getReference()

    val TAG = "DASHBOARD"

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val mapFragment: SupportMapFragment  = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return root
    }

    /**
     * Metodo di cui si è fatto l'override per permettere di eseguire delle operazioni specifiche: viene controllato
     * che l'utente abbia dato i permessi per l'accesso alla propria posizione e nel caso chiedere il permesso. Successivamente
     * viene impostata la mappa affinchè venga mostrata la posizione attuale dell'utente. In questo metodo è anche
     * impostato un lister sul marker che permetta di visualizzare una finestra pop-up.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.clear()

        val permission = ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
        } else {
            requestPermission(
                Manifest.permission.ACCESS_FINE_LOCATION,
                LOCATION_REQUEST_CODE)
        }

        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    latitude = location.latitude
                    longitude = location.longitude
                    currentLocation = LatLng(location.latitude, location.longitude)
                }
            }.addOnFailureListener {
                latitude = 44.724065
                longitude = 8.854664
                currentLocation = LatLng(latitude, longitude)
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
            }.addOnCompleteListener {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
            }


        mMap.setOnMarkerClickListener {
            showAlert(it.tag as Int)
            true
        }

    }

    /**
     * Metodo utilizzato per chiedere il permesso di accedere alla posizione.
     */
    private fun requestPermission(permissionType: String, requestCode: Int) {
        ActivityCompat.requestPermissions(
            requireActivity(), arrayOf(permissionType), requestCode
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStart() {
        super.onStart()
        button_search_dashboard.setOnClickListener {
            listItems.clear()
            mMap.clear()
            search(search_bar_dashboard.text.toString())
        }
    }

    /**
     * Metodo di ricerca. Questo controlla che siano presenti luoghi corrispondenti alla stringa passata:
     * nel caso siano presenti imposta le coordinate relative e richiama il metodo per la ricerca degli item utilizzando
     * il luogo; altrimenti si richiama il metodo per la ricerca degli item utilizzando una parola chiave.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun search(name: String){
        var geocodeMatches: List<Address>? = null

        try {
            geocodeMatches = Geocoder(requireContext())
                .getFromLocationName(name,
                    1)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (geocodeMatches != null) {
            if(geocodeMatches.size != 0) {
                latitude = geocodeMatches[0].latitude
                longitude = geocodeMatches[0].longitude
                searchItemFromLocation()
            }else{
                searchItemFromName(name)
            }
        }
    }

    /**
     * Metodo di recupero degli item dal database utilizzando come parametro di ricerca una parola chiave che corrisponda
     * con una di quelle presenti in ogni item. In questo metodo vengono anche fatti il controllo che la distanza a cui si trova
     * l'utente rispetto al luogo dell'item rientri nel raggio impostato e che la data in cui si effettua la
     * ricerca non abbia superato la data di scadenza della disponibilità di un item. Vengono inoltre mostrati solo gli
     * item che siano disponibili e quindi non già impegnati in uno share.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun searchItemFromName(name: String) {
        db.setFirestoreSettings(settings)
        db.collection("items")
            .whereEqualTo("availability", true)
            .whereArrayContains("keys", name.toUpperCase())
            .get()
            .addOnSuccessListener {
                val tmp = it.documents
                for (i in 0..tmp.size - 1) {
                    val obj = tmp.get(i).data
                    val dateExpire = LocalDate.parse(obj!!.get("dataExpire").toString())
                    val latitudeObj = obj.get("latitude").toString().toDouble()
                    val longitudeObj = obj.get("longitude").toString().toDouble()
                    val latlonObj = LatLng(latitudeObj, longitudeObj)
                    if((calculateBound(LatLng(latitude, longitude), latlonObj) <=
                        distance_search_dashboard.text.toString().toInt()) && dateExpire.isAfter(LocalDate.now())){
                        listItems.add(
                            ImageCustom(
                                ByteArray(0),
                                obj.get("name").toString(),
                                obj.get("availability").toString(),
                                obj.get("idOwner").toString(),
                                obj.get("idItems").toString(),
                                latitudeObj,
                                longitudeObj,
                                obj.get("dataExpire").toString(),
                                obj.get("keys").toString()
                            )
                        )

                        mMap.addMarker(MarkerOptions()
                            .position(latlonObj)
                            .title(obj.get("name").toString())).tag = i
                    }
                }

            }.addOnFailureListener {
                Log.d(TAG, "------------Errore recupero dati")
            }
            .addOnCompleteListener {
                getImage()
            }
    }

    /**
     * Metodo di ricerca del nome di una località date le coordinate
     */
    private fun searchLocationFromCoordinates(latLng: LatLng) : List<Address>?{
        var geocodeMatches: List<Address>? = null
        try {
            geocodeMatches = Geocoder(requireContext()).getFromLocation(latLng.latitude, latLng.longitude, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return geocodeMatches
    }

    /**
     * Metodo di recupero degli item dal database utilizzando come parametro di ricerca le coordinate della
     * località cercata. In questo metodo vengono anche fatti il controllo che la distanza a cui si trova
     * l'utente rispetto al luogo dell'item rientri nel raggio impostato e che la data in cui si effettua la
     * ricerca non abbia superato la data di scadenza della disponibilità di un item. Vengono inoltre mostrati solo gli
     * item che siano disponibili e quindi non già impegnati in uno share.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun searchItemFromLocation(){
        db.setFirestoreSettings(settings)
        db.collection("items")
            .whereEqualTo("availability", true)
            .get()
            .addOnSuccessListener {
                val tmp = it.documents
                for (i in 0..tmp.size - 1) {
                    val obj = tmp.get(i).data
                    val dateExpire = LocalDate.parse(obj!!.get("dataExpire").toString())
                    val latitudeObj = obj.get("latitude").toString().toDouble()
                    val longitudeObj = obj.get("longitude").toString().toDouble()
                    val latlonObj = LatLng(latitudeObj, longitudeObj)
                    if(calculateBound(LatLng(latitude, longitude), latlonObj) <=
                        distance_search_dashboard.text.toString().toInt() && dateExpire.isAfter(LocalDate.now())){
                        listItems.add(
                            ImageCustom(
                                ByteArray(0),
                                obj.get("name").toString(),
                                obj.get("availability").toString(),
                                obj.get("idOwner").toString(),
                                obj.get("idItems").toString(),
                                latitudeObj,
                                longitudeObj,
                                obj.get("dataExpire").toString(),
                                obj.get("keys").toString()
                            )
                        )

                        mMap.addMarker(MarkerOptions()
                            .position(latlonObj)
                            .title(obj.get("name").toString())).tag = i
                    }
                }

            }.addOnFailureListener {
                Log.d(TAG, "------------Errore recupero dati")
            }
            .addOnCompleteListener {
                getImage()
            }
    }

    /**
     * Metodo di recupero delle immagini per ogni item presente nella lista recuperata a seguito della ricerca.
     */
    private fun getImage(){
        for (i in 0 .. listItems.size-1){
            val islandRef =  storageRef.child(listItems.get(i).idOwner + "/" + listItems.get(i).idItem)
            val ONE_MEGABYTE: Long = 1024 * 1024 * 5

            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
                listItems.get(i).image = it
            }.addOnFailureListener {
                // Handle any errors
                Log.d(TAG, "------------Errore recupero immagine")
            }.addOnCompleteListener {
                getListItem()
            }
        }
    }



    /**
     * Metodo che, data una lista di item, permette la visualizzazione all'utente della lista degli item.
     * Per ogni elemento della lista è impostato un listener che permette di visualizzare il fragment relativo
     * alle informazioni dell'item selezionato.
     */
    private fun getListItem(){
        val itemAdapter = ItemAdapter(
            this.requireContext(),
            listItems
        )
        listView_dashboard.adapter = itemAdapter
        listView_dashboard.setOnItemClickListener { parent, view, position, id ->
            toFragmentItemInfo(position)
        }
    }

    /**
     * Metodo che permette di passare al fragment della visualizzazione delle informazioni di un item
     */
    private fun toFragmentItemInfo(positionObject: Int){
        setFragmentResult("idItem",
            bundleOf("idItem" to listItems.get(positionObject).idItem)
        )
        val infoFragment = ItemInfo()
        val fragmentTransaction = this.parentFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.nav_host_fragment, infoFragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    /**
     * Metodo per il calcolo della distanza in chilometri tra due punti
     */
    private fun calculateBound(p: LatLng, t: LatLng): Double{
        val lon1: Double = Math.toRadians(p.longitude);
        val lat1: Double = Math.toRadians(p.latitude);
        val lon2: Double = Math.toRadians(t.longitude);
        val lat2: Double = Math.toRadians(t.latitude);
        val dlon: Double = lon2 - lon1;
        val dlat: Double = lat2 - lat1;
        val a: Double = Math.pow(Math.sin(dlat / 2), 2.0) +
            Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2.0);
        val c: Double = 2 * Math.asin(Math.sqrt(a));
            return (6367 * c)
    }

    /**
     * Metodo che permette di visualizzare un messaggio pop-up nel quale si mostrano le informazioni
     * relative al punto in cui il marker è impostato. Da questo pop-up è possibile passare al fragment
     * relativo alle informazioni dell'item
     */
    private fun showAlert(positionObject: Int) {
        val obj = listItems.get(positionObject)
        val nameLocation = searchLocationFromCoordinates(LatLng(obj.latitude,obj.longitude))
        AlertDialog.Builder(requireContext())
            .setTitle("Demo")
            .setMessage("Nome: " + obj.name +
                        "\tLuogo: " + nameLocation?.get(0)!!.getAddressLine(0))
            .setPositiveButton("Info") { _: DialogInterface, _: Int ->
               toFragmentItemInfo(positionObject)
            }
            .show()
    }

    override fun onResume() {
        super.onResume()
        search_bar_dashboard.text.clear()
        distance_search_dashboard.setText("1")
    }


}
