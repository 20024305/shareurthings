package it.cepollina.shareurthings.ui.items

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.FirebaseStorage
import it.cepollina.shareurthings.R
import it.cepollina.shareurthings.adapter.item.ImageCustom
import it.cepollina.shareurthings.adapter.item.ItemAdapter
import kotlinx.android.synthetic.main.items_fragment.*
import it.cepollina.shareurthings.ui.items.ItemAddFragment as ItemAddFragmen

class ItemsFragment : Fragment() {


    val db = FirebaseFirestore.getInstance()
    val settings = FirebaseFirestoreSettings.Builder()
        .setTimestampsInSnapshotsEnabled(true)
        .build()
    val listItems = ArrayList<ImageCustom> ()

    val auth = FirebaseAuth.getInstance()
    val storageRef = FirebaseStorage.getInstance().getReference()

    val TAG = "ITEM_LIST_F"

    private lateinit var viewModel: ItemsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.items_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onStart() {
        super.onStart()
        val addFragment = ItemAddFragmen()

        button_add_item.setOnClickListener {
            val fragmentTransaction = this.parentFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.nav_host_fragment, addFragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
    }

    /**
     * Metodo per il prelevamento dell'immagine di un determinato item dal database. Una volta recuperato lo inserisce nell'oggetto
     * utilizzato per la visualizzazione della lista degli item.
     */
    private fun getImage(){
        for (i in 0 .. listItems.size-1){
            var islandRef =  storageRef.child(auth.uid + "/" + listItems.get(i).idItem)
            val ONE_MEGABYTE: Long = 1024 * 1024 *5

            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
                listItems.get(i).image = it
            }.addOnFailureListener {
                // Handle any errors
                Log.d(TAG, "------------Errore recupero immagine")
            }.addOnCompleteListener {
                getListItem()
            }
        }
    }


    /**
     * Metodo per il recupero delle informazioni degli item dell'utente che ha effettuato l'accesso. Prima di prelevare
     * dal database gli item viene controllato che la lista di item presenti in memoria non sia la stessa, per evitare di
     * dover ripetere il recupero di tutti gli item dal database.
     */
    private fun getItemInfo(){
        var flag = false
        db.setFirestoreSettings(settings)
        db.collection("items")
            .whereEqualTo("idOwner", auth.currentUser!!.uid)
            .get()
            .addOnSuccessListener {
                val tmp = it.documents
                if(tmp.size == listItems.size){
                    getListItem()
                    flag = true
                }else{
                    listItems.clear()
                    for (i in 0..tmp.size - 1) {
                        val obj = tmp.get(i).data
                        listItems.add(
                            ImageCustom(
                                ByteArray(0),
                                obj!!.get("name").toString(),
                                obj.get("availability").toString(),
                                obj.get("idOwner").toString(),
                                obj.get("idItems").toString(),
                                obj.get("latitude").toString().toDouble(),
                                obj.get("longitude").toString().toDouble(),
                                obj.get("dataExpire").toString(),
                                obj.get("keys").toString()
                            )
                        )
                    }
                }
            }.addOnFailureListener {
                Log.d(TAG, "------------Errore recupero dati")
            }
            .addOnCompleteListener {
                if(!flag){
                    getImage()
                }
            }

    }

    /**
     * Metodo che, data una lista di item, permette la visualizzazione all'utente della lista degli item.
     * Per ogni elemento della lista è impostato un listener che permette di visualizzare il fragment relativo
     * alle informazioni dell'item selezionato.
     */
    private fun getListItem(){
        val itemAdapter = ItemAdapter(
            this.requireContext(),
            listItems
        )
        listView.adapter = itemAdapter
        listView.setOnItemClickListener { parent, view, position, id ->
            toFragmentItemInfo(position)
        }
    }

    /**
     * Metodo che permette di passare al fragment della visualizzazione delle informazioni di un item
     */
    private fun toFragmentItemInfo(positionObject: Int){
        setFragmentResult("idItem",
            bundleOf("idItem" to listItems.get(positionObject).idItem)
        )
        val infoFragment = ItemInfo()
        val fragmentTransaction = this.parentFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.nav_host_fragment, infoFragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun onResume() {
        super.onResume()
        getItemInfo()
    }

}
