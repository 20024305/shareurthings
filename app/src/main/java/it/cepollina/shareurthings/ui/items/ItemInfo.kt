package it.cepollina.shareurthings.ui.items

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.FirebaseStorage
import it.cepollina.shareurthings.R
import it.cepollina.shareurthings.adapter.item.ImageCustom
import it.cepollina.shareurthings.ui.share.ShareFragment
import kotlinx.android.synthetic.main.fragment_item_add.*
import kotlinx.android.synthetic.main.fragment_item_info.*
import java.io.IOException
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList

class ItemInfo : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    val db = FirebaseFirestore.getInstance()
    private lateinit var listDocument: Task<QuerySnapshot>
    val settings = FirebaseFirestoreSettings.Builder()
        .setTimestampsInSnapshotsEnabled(true)
        .build()

    val auth = FirebaseAuth.getInstance()
    val storageRef = FirebaseStorage.getInstance().getReference()

    var latitude: Double = 0.0
    var longitude: Double = 0.0

    val TAG = "ITEM_INFO_F"
    val REQUEST_IMAGE_GET = 1
    val REQUEST_IMAGE_CAPTURE = 2
    val PERMISSION_CODE = 3
    private val LOCATION_REQUEST_CODE = 101


    var uriImage: Uri = Uri.EMPTY
    var idItem: String? = null
    var idOwner: String? = null
    var idDocument: String? = null
    var itemUserLogged: Boolean = true
    var currentLocation : LatLng? = null
    var searched: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_item_info, container, false)
        val mapFragment: SupportMapFragment = childFragmentManager
            .findFragmentById(R.id.map_item_info) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return root
    }

    /**
     * Metodo di cui si è fatto l'override per permettere di eseguire delle operazioni specifiche: viene controllato
     * che l'utente abbia dato i permessi per l'accesso alla propria posizione e nel caso chiedere il permesso. Successivamente
     * viene impostata la mappa affinchè venga mostrata la posizione attuale dell'utente.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true

        val permission = ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
        } else {
            requestPermission(
                Manifest.permission.ACCESS_FINE_LOCATION,
                LOCATION_REQUEST_CODE)
        }

        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    latitude = location.latitude
                    longitude = location.longitude
                    currentLocation = LatLng(location.latitude, location.longitude)
                }
            }.addOnFailureListener {
                latitude = 44.724065
                longitude = 8.854664
                currentLocation = LatLng(latitude, longitude)
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
            }.addOnCompleteListener {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
            }

    }

    /**
     * Metodo utilizzato per chiedere il permesso di accedere alla posizione.
     */
    private fun requestPermission(permissionType: String, requestCode: Int) {
        ActivityCompat.requestPermissions(
            requireActivity(), arrayOf(permissionType), requestCode
        )
    }

    /**
     * Metodo per la ricerca di una località data il nome. Se trovata viene mostrato il marker relativo ad essa
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun searchLocationFromName(name: String){
        var geocodeMatches: List<Address>? = null

        try {
            geocodeMatches = Geocoder(requireContext())
                .getFromLocationName(name,
                    1)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (geocodeMatches != null) {
            if(geocodeMatches.size != 0) {
                latitude = geocodeMatches[0].latitude
                longitude = geocodeMatches[0].longitude
                val position = LatLng(latitude, longitude)
                mMap.addMarker(
                    MarkerOptions()
                        .position(position))
                searched = true
            }else{
                Toast.makeText(requireContext(), "Specificare un luogo esistente", Toast.LENGTH_LONG).show()
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStart() {
        super.onStart()
        name_item_info.isEnabled = false
        keys_item_item_info.isEnabled = false
        setFragmentResultListener("idItem") { key, bundle ->
            idItem = bundle.getString("idItem")
            getItemInfo()
        }

        button_modify_item_info.setOnClickListener {
            enableField()
        }

        button_remove_item_info.setOnClickListener {
            removeItem()
        }
        button_save_item_info.setOnClickListener {
            updateItem()
        }
        button_image_item_info.setOnClickListener {
            uploadImage()
        }
        button_share_item_info.setOnClickListener {
            createShare()
            availability_item_info.text = false.toString()
        }
        button_search_item_info.setOnClickListener {
            searchLocationFromName(search_bar_item_info.text.toString())
        }
        button_photo_item_info.setOnClickListener {
            val permissionCamera = ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CAMERA)
            val permissionStorage = ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

            if (permissionCamera == PackageManager.PERMISSION_GRANTED ) {
                if (permissionStorage == PackageManager.PERMISSION_GRANTED) {
                    makePhoto()
                } else {
                    requestPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        PERMISSION_CODE
                    )
                }
            } else {
                requestPermission(
                    Manifest.permission.CAMERA,
                    REQUEST_IMAGE_CAPTURE)
            }

        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        /*init()*/
        getItemInfo()
    }


   /* private fun init(){
        name_item_info.isEnabled = false
        keys_item_item_info.isEnabled = false
        setFragmentResultListener("idItem") { key, bundle ->
            idItem = bundle.getString("idItem")
            getItemInfo()
        }

        button_modify_item_info.setOnClickListener {
            enableField()
        }

        button_remove_item_info.setOnClickListener {
            removeItem()
        }
        button_save_item_info.setOnClickListener {
            updateItem()
        }
        button_image_item_info.setOnClickListener {
            uploadImage()
        }
        button_share_item_info.setOnClickListener {
            createShare()
            availability_item_info.text = false.toString()
        }
        button_search_item_info.setOnClickListener {
            searchLocationFromName(search_bar_item_info.text.toString())
        }
        button_photo_item_info.setOnClickListener {
            val permissionCamera = ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CAMERA)
            val permissionStorage = ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

            if (permissionCamera == PackageManager.PERMISSION_GRANTED ) {
                if (permissionStorage == PackageManager.PERMISSION_GRANTED) {
                    makePhoto()
                } else {
                    requestPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        PERMISSION_CODE
                    )
                }
            } else {
                requestPermission(
                    Manifest.permission.CAMERA,
                    REQUEST_IMAGE_CAPTURE)
            }

        }
    }
*/
    /**
     * Metodo per la creazione di uno share, salvando sul databse tutte le informazioni necessarie ad esso. Quando
     * l'operazione è avvenuta con successo ed è terminata viene mostrato il fragment relativo allo share appena avviato.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createShare() {
        val idShare = UUID.randomUUID().toString()
        val info = hashMapOf<String, Any?>(
            "idShare" to idShare,
            "idOwnerShare" to auth.currentUser!!.uid,
            "idItem" to idItem,
            "idOwnerItem" to idOwner,
            "startShare" to LocalDate.now().toString(),
            "endShare" to "-",
            "state" to "In corso",
            "rating_user" to 0,
            "rating_owner" to 0
        )


        db.setFirestoreSettings(settings)
        db.collection("shares")
            .add(info)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                setFragmentResult("idShare",
                    bundleOf("idShare" to idShare)
                )
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
            .addOnCompleteListener {
                db.collection("items")
                    .whereEqualTo("idItems", idItem)
                    .get()
                    .addOnSuccessListener { documents ->
                        if(documents.size() == 1){
                            documents.documents.get(0)
                                .reference
                                .update("availability", false)
                                .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!") }
                                .addOnFailureListener { e -> Log.w(TAG, "---------------Errore caricamento modifica disponibilità", e) }
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.w(TAG, "Error getting documents: ", exception)
                    }.addOnCompleteListener {
                        toShareInfo()
                    }
            }
    }

    /**
     * Metodo che permette di passare al fragment relativo alle informazioni dello share appena avviato.
     */
    private fun toShareInfo() {
        setFragmentResult("idItemShare",
            bundleOf("idItemShare" to idItem)
        )
        val shareFragment = ShareFragment()
        val fragmentTransaction = this.parentFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.nav_host_fragment, shareFragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }


    /**
     * Metodo per il recupero delle informazioni dal database dell'item e conseguente impostazione dei campi relativi
     * per la visualizzazione di esse.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun getItemInfo(){
        db.setFirestoreSettings(settings)
        db.collection("items")
            .whereEqualTo("idItems", idItem)
            .get()
            .addOnSuccessListener {
                if (it.documents.size == 1){
                    idDocument = it.documents.get(0).id
                    val item = it.documents.get(0).data
                    name_item_info.setText(item!!.get("name").toString())
                    availability_item_info.text = item.get("availability").toString()
                    keys_item_item_info.setText(item.get("keys").toString())
                    val date = LocalDate.parse(item.get("dataExpire").toString())
                    date_picker_item_info.updateDate(date.year, date.monthValue-1, date.dayOfMonth)
                    idOwner = item.get("idOwner").toString()
                    if (!idOwner.equals(auth.currentUser!!.uid)){
                        itemUserLogged = false
                        button_modify_item_info.visibility = View.INVISIBLE
                        button_remove_item_info.visibility = View.INVISIBLE
                        if (availability_item_info.text.equals("true")) {
                            button_share_item_info.visibility = View.VISIBLE
                        }
                    }
                }
            }
            .addOnFailureListener {
                Log.d(TAG, "-------------------------Errore recupero info base")
            }.addOnCompleteListener {
                getUserInfo()
            }
    }

    /**
     * Metodo per il recupero delle informazioni dal database dell'utente proprietario dell'item, con conseguente impostazione
     * dei campi relativi alla loro visualizzazione.
     */
    private fun getUserInfo(){
        db.collection("users")
            .whereEqualTo("idUser", idOwner)
            .get()
            .addOnSuccessListener {
                if (it.documents.size == 1){
                    var owner = it.documents.get(0).data
                    var infoOwner = owner!!.get("name").toString() + " " + owner.get("surname").toString()
                    owner_item_info.text = infoOwner
                }
            }
            .addOnFailureListener {
                Log.d(TAG, "--------------------------------Errore recupero info proprietario")
            }
            .addOnCompleteListener {
                getImage()
            }
    }

    /**
     * Metodo per il recupero dell'immagine relativa dell'item selezionato di cui si visualizzano le informazioni, con la
     * conseguente impostazione del campo relativo.
     */
    private fun getImage(){
        val islandRef =  storageRef.child(idOwner + "/" + idItem)
        val ONE_MEGABYTE: Long = 1024 * 1024 * 5
        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeByteArray(it, 0,  it.size)
            image_item_info.setImageBitmap(bitmap)
        }.addOnFailureListener {
            Log.d(TAG, "------------Errore recupero immagine")
        }

    }


    /**
     * Metodo per la rimozione dell'item dal database. Questo metodo è accessibile solo se l'utente che prende visione dell'item
     * ne è il proprietario.
     */
    private fun removeItem() {
        db.setFirestoreSettings(settings)
        db.collection("items")
            .document(idDocument!!)
            .delete()
            .addOnFailureListener {
                Log.d(TAG, "-------------------------Errore cancellazione Item")
            }.addOnCompleteListener {
                removeImageItem()
            }
        parentFragmentManager.popBackStack()

    }

    /**
     * Metoto per la rimozione dell'immagine dell'item che si elimina
     */
    private fun removeImageItem(){
        val islandRef =  storageRef.child(idOwner + "/" + idItem)
        islandRef.delete()
            .addOnSuccessListener {
                Log.d(TAG, "Immagine eliminata dal database")
            }.addOnFailureListener {
                Log.d(TAG, "Errore nell'eliminazione dell'immagine dal database")
            }
    }

    /**
     * Metodo per l'abilitazione dei campi che permettono la modifica di un item.
     */
    private fun enableField() {
        name_item_info.isEnabled = true
        keys_item_item_info.isEnabled = true

        keys_item_item_info.setText(
            remove_brackets(
                keys_item_item_info.text.toString()))


        button_modify_item_info.isEnabled = false
        button_modify_item_info.visibility = View.INVISIBLE
        button_remove_item_info.isEnabled = false
        button_remove_item_info.visibility = View.INVISIBLE

        button_save_item_info.isEnabled = true
        button_save_item_info.visibility = View.VISIBLE
        button_image_item_info.isEnabled = true
        button_image_item_info.visibility = View.VISIBLE
        button_photo_item_info.isEnabled = true
        button_photo_item_info.visibility = View.VISIBLE
        linear_layout_search_bar_item_info.visibility = View.VISIBLE
        map_item_info.visibility = View.VISIBLE

        date_expire_label_item_info.visibility = View.VISIBLE
        date_picker_item_info.visibility = View.VISIBLE

    }

    /**
     * Metodo per la rimozione dalla stringa presa in input delle parentesi quadre e degli spazi bianchi (se presenti)
     */
    private fun remove_brackets(string: String):String {
        var s: String = ""
        for (i in 1..string.length-2)
            if(string.toCharArray()[i] != ' ')
                s += string.toCharArray()[i]
        return s
    }

    /**
     * Metodo per l'aggiornamento all'interno del database dell'item utilizzando le nuove informazioni inserite.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun updateItem() {
        db.collection("items")
            .whereEqualTo("idItems", idItem)
            .get()
            .addOnSuccessListener { documents ->
                if(documents.size() == 1){
                    documents.documents.get(0)
                        .reference
                        .update("name", name_item_info.text.toString())
                        .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!") }
                        .addOnFailureListener { e -> Log.w(TAG, "---------------Errore caricamento modifica nome", e) }
                    documents.documents.get(0)
                        .reference
                        .update("keys", setKeysArray(keys_item_item_info.text.toString()))
                        .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!")  }
                        .addOnFailureListener {  e -> Log.w(TAG, "---------------Errore caricamento modifica chiavi", e) }
                    if(searched){
                        documents.documents.get(0)
                            .reference
                            .update("latitude", latitude)
                            .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!")  }
                            .addOnFailureListener {  e -> Log.w(TAG, "---------------Errore caricamento modifica latitudine", e) }
                        documents.documents.get(0)
                            .reference
                            .update("longitude", longitude)
                            .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!")  }
                            .addOnFailureListener {  e -> Log.w(TAG, "---------------Errore caricamento modifica longitudine", e) }
                    }
                    documents.documents.get(0)
                        .reference
                        .update("dataExpire", LocalDate.of(date_picker_item_info.year,
                            date_picker_item_info.month+1,
                            date_picker_item_info.dayOfMonth).toString())
                        .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!")  }
                        .addOnFailureListener {  e -> Log.w(TAG, "---------------Errore caricamento data di scadenza", e) }
                }
            }
            .addOnFailureListener{ exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }.addOnCompleteListener{
                if(uriImage != null){
                    val file = uriImage
                    val riversRef = storageRef.child(auth.currentUser!!.uid +
                            "/" + idItem)
                    var uploadTask = riversRef.putFile(file!!)
                    uploadTask.addOnFailureListener {
                        Log.d(TAG, "Errore caricamento immagine --------------")
                    }
                }
                parentFragmentManager.popBackStack()
            }

    }

    /**
     * Metodo per restituire una lista di stringhe data una stringa.
     */
    private fun setKeysArray(keys: String): List<String>{
        val split = keys.split(",")
        var array: ArrayList<String>? = ArrayList()
        for (i in 0..split.size-1){
            array!!.add(split.get(i).trim().toUpperCase())
        }
        return array!!
    }

    /**
     * Metodo per l'avvio della camera.
     */
    private fun makePhoto(){

        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        uriImage = requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriImage)
        if (cameraIntent.resolveActivity(requireContext().packageManager) != null) {
            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE)
        }

    }

    /**
     * Metodo per il prelevamento dell'immagine dalla galleria del dispositivo.
     */
    private fun uploadImage() {
        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "image/*"
        }
        if (intent.resolveActivity(requireContext().packageManager) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_GET)
        }
    }


    /**
     * Metodo di cui si è fatto l'override in modo da poter determinare le azioni che deve compiere questo all'interno
     * dell'applicazione. Questo nel particolare controlla se il risultato dell'intent sia quello derivato dal recupero
     * dell'immagine dalla memoria locale oppure dalla camera a seguito dello scatto di una foto: nel primo caso estrae l'uri
     * dall'immagine recuperata e la mostra nell'interfaccia; nel secondo invece la mostra direttamente siccome l'uri lo abbiamo
     * generato.
     */
    @RequiresApi(Build.VERSION_CODES.P)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_GET && resultCode == Activity.RESULT_OK && data != null) {
            val fullPhotoUri: Uri? = data.data
            if(fullPhotoUri != null){
                uriImage = fullPhotoUri
            }
            val bitmap: Bitmap =
                ImageDecoder.
                decodeBitmap(
                    ImageDecoder.createSource(requireContext().contentResolver,
                    fullPhotoUri!!
                ))
            image_item_info.setImageBitmap(bitmap)
        }else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK){
            val bitmap: Bitmap =
                ImageDecoder.
                decodeBitmap(ImageDecoder.createSource(requireContext().contentResolver,
                    uriImage
                ))
            image_item_info.setImageBitmap(bitmap)
        }
    }

}