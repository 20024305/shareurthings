package it.cepollina.shareurthings.ui.items

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.hardware.Camera
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.storage.FirebaseStorage
import it.cepollina.shareurthings.R
import it.cepollina.shareurthings.SignUpActivity
import kotlinx.android.synthetic.main.fragment_item_add.*
import kotlinx.android.synthetic.main.item_base.*
import java.io.File
import java.io.IOException
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class ItemAddFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    val db = FirebaseFirestore.getInstance()
    val settings = FirebaseFirestoreSettings.Builder()
        .setTimestampsInSnapshotsEnabled(true)
        .build()

    val auth = FirebaseAuth.getInstance()

    var latitude: Double = 0.0
    var longitude: Double = 0.0

    val TAG = "ITEM_ADD_F"
    val REQUEST_IMAGE_GET = 1
    val REQUEST_IMAGE_CAPTURE = 2
    val PERMISSION_CODE = 3
    private val LOCATION_REQUEST_CODE = 101

    var uriImage : Uri = Uri.EMPTY
    var currentLocation : LatLng? = null
    var searched: Boolean = false


    val storageRef = FirebaseStorage.getInstance().getReference()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_item_add, container, false)
        val mapFragment: SupportMapFragment  = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return root
    }

    /**
     * Metodo di cui si è fatto l'override per permettere di eseguire delle operazioni specifiche: viene controllato
     * che l'utente abbia dato i permessi per l'accesso alla propria posizione e nel caso chiedere il permesso. Successivamente
     * viene impostata la mappa affinchè venga mostrata la posizione attuale dell'utente.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true

        val permission = ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
        } else {
            requestPermission(
                Manifest.permission.ACCESS_FINE_LOCATION,
                LOCATION_REQUEST_CODE)
        }

        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    latitude = location.latitude
                    longitude = location.longitude
                    currentLocation = LatLng(location.latitude, location.longitude)
                }
            }.addOnFailureListener {
                latitude = 44.724065
                longitude = 8.854664
                currentLocation = LatLng(latitude, longitude)
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
            }.addOnCompleteListener {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
            }
    }

    /**
     * Metodo per la ricerca di una località data il nome. Se trovata viene mostrato il marker relativo ad essa
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun searchLocationFromName(name: String){
        var geocodeMatches: List<Address>? = null

        try {
            geocodeMatches = Geocoder(requireContext())
                .getFromLocationName(name,
                    1)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (geocodeMatches != null) {
            if(geocodeMatches.size != 0) {
                latitude = geocodeMatches[0].latitude
                longitude = geocodeMatches[0].longitude
                val position = LatLng(latitude, longitude)
                mMap.addMarker(
                    MarkerOptions()
                        .position(position))
                searched = true
            }else{
                Toast.makeText(requireContext(), "Specificare un luogo esistente", Toast.LENGTH_LONG).show()
            }

        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStart() {
        super.onStart()
        button_create_item.setOnClickListener {
            if (fieldCompiled())
                save_item()
            else
                Toast.makeText(requireContext(),
                    "Campi vuoti, compilarli prima di procedere al salvataggio",
                    Toast.LENGTH_LONG)
                    .show()
        }
        button_upload_image.setOnClickListener {
            uploadImage()
        }
        button_camera_image.setOnClickListener {
            //Viene controllato se l'applicazione ha il permesso di utilizzare la telecamera e di scrivere sulla memoria: in
            //caso di risposta negativa viene chiesto all'utente il permesso
            val permissionCamera = ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CAMERA)
            val permissionStorage = ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

            if (permissionCamera == PackageManager.PERMISSION_GRANTED ) {
                if (permissionStorage == PackageManager.PERMISSION_GRANTED) {
                    makePhoto()
                } else {
                    requestPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        PERMISSION_CODE
                    )
                }
            } else {
                requestPermission(
                    Manifest.permission.CAMERA,
                    REQUEST_IMAGE_CAPTURE)
            }

        }
        button_search_item_add.setOnClickListener {
            searchLocationFromName(search_bar_item_add.text.toString())
        }
    }

    /**
     * Metodo di controllo dei campi se vuoti
     */
    private fun fieldCompiled(): Boolean {
        return !name_field_item.text.isBlank() &&
                !keys_item_field_item_add.text.isBlank() &&
                uriImage != Uri.EMPTY
    }


    /**
     * Metodo per il salvataggio sul database dei dati riguardanti l'item
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun save_item() {
        var idItems = UUID.randomUUID().toString()
        val info = hashMapOf<String, Any?>(
            "idOwner" to auth.currentUser!!.uid,
            "idItems" to idItems,
            "name" to name_field_item.text.toString(),
            "keys" to setKeysArray(keys_item_field_item_add.text.toString()),
            "availability" to true,
            "latitude" to latitude,
            "longitude" to longitude,
            "dataExpire" to LocalDate.of(date_picker_item_add.year,
                date_picker_item_add.month+1,
                date_picker_item_add.dayOfMonth).toString()
        )

        var file = uriImage


        db.setFirestoreSettings(settings)
        db.collection("items")
            .add(info)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                val riversRef = storageRef.child(auth.currentUser!!.uid +
                        "/" + idItems)
                var uploadTask = riversRef.putFile(file)
                uploadTask.addOnFailureListener {
                    Log.d(TAG, "Errore caricamento immagine --------------")
                }.addOnSuccessListener { taskSnapshot ->
                    parentFragmentManager.popBackStack()
                }
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }

    }

    /**
     * Metodo per l'avvio della camera.
     */
    private fun makePhoto(){

        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        uriImage = requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriImage)
        if (cameraIntent.resolveActivity(requireContext().packageManager) != null) {
            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE)
        }

    }

    /**
     * Metodo per il recupero di una immagine dalla memoria locale del dispositivo
     */
    private fun uploadImage() {
        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "image/*"
        }
        if (intent.resolveActivity(requireContext().packageManager) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_GET)
        }
    }


    /**
     * Metodo di cui si è fatto l'override in modo da poter determinare le azioni che deve compiere questo all'interno
     * dell'applicazione. Questo nel particolare controlla se il risultato dell'intent sia quello derivato dal recupero
     * dell'immagine dalla memoria locale oppure dalla camera a seguito dello scatto di una foto: nel primo caso estrae l'uri
     * dall'immagine recuperata e la mostra nell'interfaccia; nel secondo invece la mostra direttamente siccome l'uri lo abbiamo
     * generato.
     */
    @RequiresApi(Build.VERSION_CODES.P)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "----------------------------------------------Aiutoooooooo Fuori")
        if ((requestCode == REQUEST_IMAGE_GET) && resultCode == Activity.RESULT_OK && data != null) {
            val fullPhotoUri: Uri? = data.data
            if(fullPhotoUri != null){
                uriImage = fullPhotoUri
            }
            val bitmap: Bitmap =
               ImageDecoder.
               decodeBitmap(ImageDecoder.createSource(requireContext().contentResolver,
                   fullPhotoUri!!
               ))
            image_load.setImageBitmap(bitmap)
        }else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK){
            Log.d(TAG, "----------------------------------------------Aiutoooooooo")
            val bitmap: Bitmap =
                ImageDecoder.
                decodeBitmap(ImageDecoder.createSource(requireContext().contentResolver,
                    uriImage
                ))
            image_load.setImageBitmap(bitmap)
        }
    }

    /**
     * Metodo utilizzato per chiedere il permesso di accedere alla posizione.
     */
    private fun requestPermission(permissionType: String, requestCode: Int) {
        ActivityCompat.requestPermissions(
            requireActivity(), arrayOf(permissionType), requestCode
        )
    }

    /**
     * Metodo per la creazione di una lista contentente le parole chiave di un item, rimuovendo eventuali spazi
     */
    private fun setKeysArray(keys: String): List<String>{
        val split = keys.split(",")
        var array: ArrayList<String>? = ArrayList()
        for (i in 0..split.size-1){
            array!!.add(split.get(i).trim().toUpperCase())
        }
        return array!!
    }

}

