package it.cepollina.shareurthings.ui.profile

import android.content.Context
import android.os.Bundle
import android.provider.ContactsContract
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import it.cepollina.shareurthings.HomeActivity
import it.cepollina.shareurthings.R
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.name_field_profile

class ProfileFragment : Fragment() {

    private lateinit var profileViewModel: ProfileViewModel
    val db = FirebaseFirestore.getInstance()
    val settings = FirebaseFirestoreSettings.Builder()
    .setTimestampsInSnapshotsEnabled(true)
    .build()

    val auth = FirebaseAuth.getInstance()
    val TAG = "PROFILE_F ACTIVITY"

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        profileViewModel =
                ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_profile, container, false)

        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getUserInfo()
    }

    /**
     * Metodo di recupero delle informazioni dal database riguardanti l'utente che ha eseguto l'accesso.
     * Dopo di questo imposta i campi relativi alla visualizzazione delle informazioni.
     */
    private fun getUserInfo(){
        db.setFirestoreSettings(settings)
        db.collection("users")
            .whereEqualTo("idUser", auth.currentUser!!.uid)
            .get()
            .addOnSuccessListener { documents ->
                if(documents.size() == 1){
                    val data = documents.documents.get(0).data
                    name_field_profile.setText(data!!.get("name").toString())
                    surname_field_profile.setText(data!!.get("surname").toString())
                }
                for (document in documents) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }

    override fun onStart() {
        super.onStart()
        surname_field_profile.isEnabled = false
        name_field_profile.isEnabled = false
        button_save.isEnabled = false

        button_modify.setOnClickListener{
            enableField(true)
        }

        button_save.setOnClickListener {
            updateInfoUser()
        }

    }

    /**
     * Metodo che permette di rendere accessibili dei campi dell'interfaccia.
     */
    fun enableField(value: Boolean){
        surname_field_profile.isEnabled = value
        name_field_profile.isEnabled = value
        button_save.isEnabled = value
        if (value){
            button_save.visibility = View.VISIBLE
        }else{
            button_save.visibility = View.INVISIBLE
        }
    }

    /**
     * Metodo di aggiornamento delle informazioni riguardanti l'utente che ha effettuato l'accesso.
     */
    fun updateInfoUser(){
        db.collection("users")
                .whereEqualTo("idUser", auth.currentUser!!.uid)
                .get()
                .addOnSuccessListener { documents ->
                    if(documents.size() == 1){
                        documents.documents.get(0)
                                .reference
                                .update("name", name_field_profile.text.toString())
                                .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!") }
                                .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }
                        documents.documents.get(0)
                                .reference
                                .update("surname", surname_field_profile.text.toString())
                                .addOnCompleteListener {  Log.d(TAG, "DocumentSnapshot successfully updated!") }
                                .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }
                    }
                    enableField(false)
                }
                .addOnFailureListener{ exception ->
                    Log.w(TAG, "Error getting documents: ", exception)
                }
    }

}
